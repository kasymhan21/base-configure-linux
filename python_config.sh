wget https://www.python.org/ftp/python/(version)/Python(version).tgz ; \ 
tar xvf Python-x.x.* ; \
cd Python-x.x.x ; \
mkdir ~/.python ; \
./configure --enable-optimizations --prefix=/home/user_name/.python ; \
make -j8 ; \
sudo make altinstall

sudo /home/user_name/.python/bin/python3.7 -m pip install -U pip
